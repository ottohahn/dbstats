\echo Use "CREATE EXTENSION dbstats" to load this file \quit

CREATE OR REPLACE FUNCTION public.get_count_by_column(col TEXT, tabl TEXT)
RETURNS table (
summary text,
counts bigint
)
/* Author: OHH
 * Date: 2019-09-09
 * Purpose: Get the counts of unique keys, nulls, not nulls, and total records for a column
 * in a table
 */
AS $$
BEGIN
  RETURN QUERY EXECUTE
  'select ''UNIQUE KEYS'' as summary, count(distinct ' || col || ') as counts from '
 	|| tabl || ' where ' || col || ' is not null
 	union all
	select ''NULLS''as summary, count(*) as counts from '
 	|| tabl || ' where ' || col || ' isnull
	union all
	select ''NOT NULLS'' as summary, count(*) as counts from '
 	|| tabl || ' where ' || col || ' is not null
	union all
	select ''TOTAL RECORDS'', count(*) from '
 	|| tabl
 ;
	END;
$$ LANGUAGE 'plpgsql';

/* Report for integers */
CREATE OR REPLACE FUNCTION public.get_report_int(col TEXT, tabl TEXT)
RETURNS table (
summary text,
val float)
/* Author: OHH
 * Date: 2019-11-04
 * Purpose: Get the max, min, mean, spread, mode, median for a column
 * in a table
 */
AS $$
BEGIN
  RETURN QUERY EXECUTE
  'select ''MAX'' as summary, max('|| col ||') as VALUE from '|| tabl ||
  'union all
  select ''MEAN'', avg(' || col ||') from ' tabl ||
  'union all
  select ''MIN'', min('|| col ||') from '|| tabl ||
  'union all
  select ''SPREAD'',abs(max('||col||')-min('|| col ||')) from ' || tabl ||
  'union all
  select ''MODE'', mode() within group (order by ' col ') from '|| tabl ||
  'union all
  select ''MEDIAN'', ROUND(PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY '|| col ||')::numeric, 2) AS median FROM '||
  tabl
  ;
END;
$$ LANGUAGE 'plpgsql';


/* Report for floats */
CREATE OR REPLACE FUNCTION public.get_report_float(col TEXT, tabl TEXT)
RETURNS table (
summary text,
val float)
/* Author: OHH
 * Date: 2019-11-04
 * Purpose: Get the max, min, mean, spread, mode, median, std_dev for a column
 * in a table
 */
AS $$
BEGIN
  RETURN QUERY EXECUTE
  'select ''MAX'' as summary, max('|| col ||') as VALUE from '|| tabl ||
  'union all
  select ''MEAN'', avg(' || col ||') from ' tabl ||
  'union all
  select ''MIN'', min('|| col ||') from '|| tabl ||
  'union all
  select ''SPREAD'',abs(max('||col||')-min('|| col ||')) from ' || tabl ||
  'union all
  select ''MODE'', mode() within group (order by ' col ') from '|| tabl ||
  'union all
  select ''MEDIAN'', ROUND(PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY '|| col ||')::numeric, 2) AS median FROM '||
  tabl || 'union all
  select ''STD DEV'', stddev_samp('|| col ||') from '|| tabl
 ;
END;
$$ LANGUAGE 'plpgsql';

/* Regression */
CREATE OR REPLACE FUNCTION public.get__regression(col1 TEXT, col2 TEXT, tabl TEXT)
RETURNS table (
summary text,
val float)
/* Author: OHH
 * Date: 2019-11-05
 * Purpose: Get a linear regression of columns col1 and col2 in tabl
 * the equation is col1 = M*col2 + B
 */
AS $$
BEGIN
  RETURN QUERY EXECUTE
  'select ''CORRELATION'' as summary,  corr(' || col1 !! ',' || col2 || ') as 
  val from ' || tabl || 
  ' union all
  select ''SAMP COVARIANCE'' as summary, covar_samp(' || col1|| ',' || col2 || 
  ') as val from' || tabl || 
  ' union all
  select ''R2 COEF'' as summary, regr_r2(' || col1 || ',' || col2 || ') as val 
  from ' || tabl ||
  ' union all
  select ''B'' as summary, regr_intercept(' || col1 || ',' || col2 || ') as val 
  from ' || tabl || 
  ' union all
  select ''M'' as summary, regr_slope(' || col1 ||  ',' ||  col2 || ') as val 
  from ' || tabl
  ;
END;
$$ LANGUAGE 'plpgsql';
