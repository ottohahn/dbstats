# dbstats

A PL/SQL PostgreSQL extension for doing quick data analytics and data quality on databases

## Contents
 
 1. Counts and data quality
 2. Descriptive statistics of one variable
 3. Descriptive statistics of two variables
 - Dependencies
 - Installation
 - Algorithms
 - Examples
 - Developers

## 1. Counts and data quality

- get_count_by_column(col TEXT, tabl TEXT)

## 2. Descriptive statistics of one variable

- get_report_int(col)
- get_report_float(col)

## 3. Descriptive statistics of two variables

- get_regression(col1, col2, tabl)

## Dependencies
The code was created using pgPL/SQL

- PostgreSQL +9.0

## Installation


In the source directory

```
$ make install
```

In the database

```
> CREATE EXTENSION dbstats;
```

## Algorithms

This is just an aggregator, all functions are taken from either the (standard library)[https://www.postgresql.org/docs/10/functions-aggregate.html] or from (this github)[https://github.com/ellisonch/PostgreSQL-Stats-Aggregate/blob/master/pg_stats_aggregate.sql]

### WIP

- Average Absolute Deviation
- Coefficient of Variation
- Interquartile Range
- Kurtosis
- Skewness
- Index of Dispersion


## Examples

To use the `get_count_by_column` use:

```
> select * from get_count_by_column('x2', 'sample1');

|summary|counts|
|-------|------|
|UNIQUE KEYS|1820|
|NULLS|123|
|NOT NULLS|1877|
|TOTAL RECORDS|2000|
```

To use the `get_report_int` function:

```
> select * from get_report_int('x1', 'sample2');

|summary|value|
|-------|-----|
|MAX    |100  |
|MEAN   |49.5 |  
|MIN    |1    |
|SPREAD |99   |
|MODE   |48   |
|MEDIAN |50   |
```

Example for `get_report_float`

```
> select * from get_report_float('x3', 'sample2');

|summary|value|
|-------|-----|
|MAX    |100  |
|MEAN   |49.5 |  
|MIN    |1    |
|SPREAD |99   |
|MODE   |48   |
|MEDIAN |50   |
|STD DEV|30   |

```

Example of `get_regression`

```
> select * from get_regression('col1', 'col2', 'mytable');

|SUMMARY          | VAL    |
|-----------------|--------|
| CORRELATION     | 0.8702 |
| SAMP COVARIANCE | 0.3045 |
| R2 COEF         | 0.8234 |
| B               | 1.2437 |
| M               | 2.7681 |

```

To create cross tabulations check the crosstab function in the tablefuncs
extension.

## Developers

Otto Hahn Herrera Ph. D.
